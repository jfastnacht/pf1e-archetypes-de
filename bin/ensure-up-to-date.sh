#!/bin/bash
# this bash script runs the JSON formatting
# it ensures that all files are formatted inside the 'translations/' folder, as well as 'module.json'
# Based on "ensure-up-to-date" script in OpenAPI Generator

echo "IMPORTANT: this script should be run by the CI (e.g. GitLab)"
echo "It ensures that the JSON-files inside the 'translations/' folder and the 'module.json' is formatted correctly."
echo "Please press CTRL+C to stop or the script will continue in 5 seconds."

sleep 5

shfolder=$(dirname "$0")

php $shfolder/jsonformat $shfolder/../module.json
php $shfolder/jsonformat $shfolder/../translations/

# Check:
if [ -n "$(git status --porcelain)" ]; then
    echo "UNCOMMITTED CHANGES ERROR"
    echo "There are uncommitted changes in working tree after execution of 'bin/ensure-up-to-date'"
    echo "Perform git diff"
    git --no-pager diff
    echo "Perform git status"
    git status
    echo "Please run 'ensure-up-to-date' locally and commit changes (UNCOMMITTED CHANGES ERROR)"
    exit 1
else
    echo "Git working tree is clean"
fi

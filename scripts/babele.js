Hooks.on('init', () => {

    if(typeof Babele !== 'undefined') {
        Babele.get().register({
            module: 'pf1e-archetypes-de',
            lang: 'de',
            dir: 'translations'
        });
    }
});

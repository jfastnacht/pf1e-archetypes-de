# Pathfinder 1e - Archetypen und Fähigkeiten

> Deutsche Übersetzung von 'pf1e-archetypes' für Foundry Virtual Tabletop

## Legal

### GNU General Public License v3.0

Every part of this repository that doesn't belong to the Paizo Community Use or FVTT Limited License for Module Development is licensed
under the GNU General Public License v3.0. 

### Paizo Community Use

> This translation uses trademarks and/or copyrights owned by Paizo Inc., used under Paizo's Community Use Policy (paizo.com/communityuse).
> We are expressly prohibited from charging you to use or access this content. This translation is not published, endorsed, or specifically
> approved by Paizo. For more information about Paizo Inc. and Paizo products, visit paizo.com.

### FoundryVTT Limited License for Module Development

> As a software license owner, you are granted license to develop software packages which extend the functionality of the software if you
> agree to all of the following terms:
> 1. You may create packages, which include game systems, add-on modules, and worlds which reference or duplicate portions of the software
> code. Publication of packages which reference or include software code and function in the absence of the base software is not permitted.
> Including portions of software code is only permitted as is strictly necessary for proper functioning of the package.
> 2. You may distribute these packages provided they are designed to be used only in conjunction with a licensed copy of the software.
> 3. You are allowed to sell or lease package content which you have the rights to distribute. You bear the sole responsibility to uphold
> intellectual property rights as required by copyright law.
> 4. The publication of package content is bound by the terms of the most recent available version of this agreement found on the Foundry
> Virtual Tabletop website. Any modifications of this policy shall not retroactively alter or revoke the rights given under the previous
> limited license. When you publish changes to an existing package, it becomes bound by the most recent terms of this agreement.
> If you do not agree to the terms of this license, you may not publish package content. Any existing packages that were created under a
> previous version of this agreement may remain published provided they adhere to the license terms under which they were published.
